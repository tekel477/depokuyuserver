const mqtt = require("mqtt");
const colors = require("colors");

const appName = "ukontrol";
const device = {
  imei: 123123123123123,
  name: "Test Cihazi 1",
  type: "sdfsf",
  sensor_type: "d",
  aIn_max: 3000,
  aIn_max: 1500,

  ext: {
    aIn_max: 3000,
    aIn_min: 1500
  },
  status: {
    output: false,
    aIn: 1234,
    dIn: false,
    rssi: 30
  }
};

device.name = "hello";
const client = mqtt.connect("mqtt://127.0.0.1", {
  clientId: String(device.imei)
});

client.on("connect", () => {
  console.log(client.options.clientId + " connected");

  const subscribtions = [
    "o", // Cihazimizin aldigi role ac/kapa komutu
    "set" // Cihaza daha sonra ayar yapilabilmesi icin
  ];

  subscribe(subscribtions);

  // Gonderilen durum bilgisi
  // output         X       1 bayt   0 | 1
  // analog input   XXXX    4 bayt   0000 - 2800
  // digital input  X       1 bayt   0 | 1
  // RSSI           XXX     3 bayt

  let data2Send;

  setInterval(() => {
    device.status.aIn = Math.floor(Math.random() * 2800 + 1000);
    data2Send =
      (device.status.output ? "1" : "0") +
      "" +
      device.status.aIn +
      "" +
      device.status.rssi;

    console.log("anlik degerler kontrol ediliyor " + data2Send);
    if (device.status.aIn < device.ext.aIn_min) {
      console.log("min degerin altina dustu".red);
      publish("ext", "0");
    }
    if (device.status.aIn > device.ext.aIn_max) {
      console.log("max degerin ustune cikti".red);
      publish("ext", "1");
    }
    // publish("feed", data2Send + "");
  }, 2500);
});

client.on("message", (topic, message) => {
  let msg = message.toString();
  console.log(`${topic} <<< ${msg}`.bgGreen);

  let cmd = topic.split("/");

  // Alinan komuta gore
  switch (cmd[2]) {
    case "o":
      if (msg === "1") {
        console.log("role acildi".green);
        device.status.output = true;
      } else {
        console.log("role kapandi".green);
        device.status.output = false;
      }
      break;
    default:
      break;
  }
});

const subscribe = subs => {
  subs.forEach(sub => {
    client.subscribe(appName + "/" + device.imei + "/" + sub);
    console.log(appName + "/" + device.imei + "/" + sub + " <<<");
  });
};

const publish = (topic, data) => {
  client.publish(appName + "/" + device.imei + "/" + topic, data + "");
  console.log(`${appName}/${device.imei}/${topic} >>> ${data}`.bgBlue);
};
