const mqtt = require("mqtt");
const colors = require("colors");

const appName = "ukontrol";

/*
  CIHAZ OZELLIKLERI
  -Cihazin bir veya daha fazla
  -Cihaz dijital cikis (do) acma kapama komutunu asagidaki gibi alir
    ukontrol/imei_no/do/1
    {uygulama_ismi}/{cihaz_imei_numarasi}/{cihaz_dijital_veya_analog_giris_cikis_tipi}/{cihaz_giris_cikis_sirasi}
    -Bu aldigi komuta karsilik 
    ukontrol/imei_no/fb/do/1
    {uygulama_ismi}/{cihaz_imei_numarasi}/fb/{cihaz_dijital_veya_analog_giris_cikis_tipi}/{cihaz_giris_cikis_sirasi}
*/

const device = {
  // serial_number: "123123123123123",
  // serial_number: "321321321321321",
  // serial_number: "345345345345345",
  serial_number: "567567567567567",

  do_status: false
};

const client = mqtt.connect("mqtt://185.122.200.117", {
  clientId: String(device.serial_number)
});

client.on("connect", () => {
  console.log(device.serial_number + " connected");
  subscribe("/do/#");
  subscribe("/data/#");
  subscribe("/set/#");

  var value_1 = 0,
    value_2 = 0;
  // setInterval(() => {
  //   publish("/fb/di/1", "di" + value_1);
  //   value_1++;
  // }, 4000);

  // setTimeout(() => {
  //   setInterval(() => {
  //     publish("/fb/ai/1", "ai" + value_2);
  //     value_2++;
  //   }, 4000);
  // }, 2000);
});

client.on("message", (topic, message) => {
  let msg = message.toString();
  console.log(`${topic} <<< ${msg}`.bgGreen);

  let cmd = topic.split("/");

  switch (cmd[2]) {
    case "do":
      if (msg == "1") {
        console.log("acildi");
        publish("/fb/do/" + cmd[3], "1");
        do_status = true;
      } else {
        console.log("kapandi");
        publish("/fb/do/" + cmd[3], "0");
        do_status = false;
      }
      break;
    default:
      console.log("komut algilanamadi");
      break;
  }
});

const subscribe = i => {
  client.subscribe(appName + "/" + device.serial_number + "" + i);
};

const publish = (t, m) => {
  client.publish(`${appName}/${device.serial_number}${t}`, m.toString());
  console.log(`${appName}/${device.serial_number}${t} >>> ${m}`.bgRed);
};
