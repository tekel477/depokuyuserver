const func = require("./func");
const sms = require("./sms");
const mail = require("./mail");
const mqtt = require('mqtt');
const axios = require('axios');


var MQTT_INFO = {
    host: "mqtt://192.168.1.2",
    clientId : "DepoKuyuServerJS"+Math.floor( Math.random() * 10000 ),
    username: "DepoKuyuWeb",
    password: "depokuyuweb",
    serverToWebDestination:"DepoKuyu/Server",
    webToServerDestination:"DepoKuyu/Web",
    UART:"UART"
};

const client = mqtt.connect(MQTT_INFO.host,
    {
        clientId: MQTT_INFO.clientId,
        port: MQTT_INFO.port,
        username: MQTT_INFO.username,
        password: MQTT_INFO.password,
        
    }
);
var depokuyu = null;
var devicesettings = null;
var acikKuyu = {};
var devices = {};
var serial_array = []; 
var statusinterval = null; //canlılık sorgusu için interval
var levelinterval= null; //saniyede bir seviye sorgusu için interval
var devicesseviye = []; //cihazdan gelen seviyeleri tutar
var set = new Set(); //Diziye ekleneni birdaha eklemiyor


function DeviceSubscribe(){
    axios.get('http://localhost/DepoKuyuWeb/public/api/getdevices')
    .then(function(response){
        response.data.forEach(item => {
            // var topic1 = item.serial + "/DKW/RES"
            var topic2 = item.serial + "/DK/RES"
            var topic3 = item.serial + "/DKW/CMD"
            var topic4 = item.serial + "UART"
            // client.subscribe(topic1)
            client.subscribe(topic2)
            client.subscribe(topic3)
            client.subscribe(topic4)
            //serial_array.push(item.serial) 
            set.add(item.serial);
            console.log(set.size === set.length);
            // console.log(set);
            deviceSettingsUpdate(item.serial,item.baslangic_saati,item.bitis_saati,item.control_time,item.pompa_debi,item.depo_height,item.depo_volume,item.depo_max_level,item.depo_min_level,item.total_working_hours,item.phone1,item.phone2,item.phone3,item.email1,item.email2,item.name);
            
            // console.log(item.serial)
        
        });
        // return response;
    })
    .catch(function(error){
        DeviceSubscribe();
        console.log(error);
    })
}
function DeviceSettings(){
    axios.get('http://localhost/DepoKuyuWeb/public/api/settings')
    .then(function(response){
        // console.log(response.data);
        devicesettings = response.data;
        // deviceSettingsUpdated(devicesettings.serial,devicesettings.baslangic_saati,devicesettings.bitis_saati,devicesettings.control_time,devicesettings.name,devicesettings.pompa_debi,devicesettings.depo_height,devicesettings.depo_max_level,devicesettings.depo_min_level,devicesettings.total_working_hours,devicesettings.phone1,devicesettings.phone2,devicesettings.phone3,devicesettings.email1,devicesettings.email2)
    })
    .catch(function(error){
        DeviceSettings();
        console.log(error);
    })
}
function DepoKuyu(){
    axios.get('http://localhost/DepoKuyuWeb/public/api/depokuyu')
    .then(function(response){
        // console.log(response.data);
        depokuyu=response.data;
    })
    //deneme
}
function deviceSettingsUpdate(serial,baslangic_saati,bitis_saati,control_time,pompa_debi,depo_height,depo_volume,depo_max_level,depo_min_level,total_working_hours,phone1,phone2,phone3,email1,email2,name){ //Bu fonksiyon ise zaman fark kontrolü için şuanı ve son işlem zamanını güncellemek için yazılmıştır.
    let currentTime = new Date().getTime();
    let last_time= new Date(currentTime-(control_time*1000*60)).getTime();
    let seviye = "";
    // console.log({currentTime,last_time})
    if(!devices[serial]) {
        devices[serial] =  {baslangic_saati:"",bitis_saati:"",control_time:"",pompa_debi:"",depo_height:"",depo_volume:"",depo_max_level:"",depo_min_level:"",total_working_hours:"",phone1:null,phone2:null,phone3:null,email1:null,email2:null,name:null,last_time:null,seviye:"",messagedurum:true};
    }
        devices[serial].baslangic_saati = baslangic_saati;
        devices[serial].bitis_saati = bitis_saati;
        devices[serial].control_time = control_time;
        devices[serial].pompa_debi = pompa_debi;
        devices[serial].depo_height = depo_height;
        devices[serial].depo_volume = depo_volume;
        devices[serial].depo_max_level = depo_max_level;
        devices[serial].depo_min_level = depo_min_level;
        devices[serial].total_working_hours = total_working_hours;
        devices[serial].phone1 = phone1;
        devices[serial].phone2 = phone2;
        devices[serial].phone3 = phone3;
        devices[serial].email1 = email1;
        devices[serial].email2 = email2;
        devices[serial].name = name;
        devices[serial].last_time = last_time;
        devices[serial].seviye = seviye;
        devices[serial].mesagedurum = true;
        // seviyeGuncelle(depo_max_level,depo_min_level);
        
        console.log({serial,name:devices[serial].name,pompa_debi:devices[serial].pompa_debi,depo_height:devices[serial].depo_height,depo_volume:devices[serial].depo_volume,depo_max_level:devices[serial].depo_max_level,depo_min_level:devices[serial].depo_min_level,total_working_hours:devices[serial].total_working_hours,seviye: devices[serial].seviye})
    
    // console.log(devices);
    
}

function istekleri_ayir(veriler){
    if(veriler && veriler.veri && veriler.konu == MQTT_INFO.webToServerDestination){
        let message = veriler.veri.toString().split("/");
        let clientrand = message[0] // randnumber/islem/serial
        let islem =message[1] 
        // if(islem=="SettingsUpdated"){
        //     console.log("Sistem Ayarları Güncellendi")  
        //     //devices_subscribe(serial)
        //     //DeviceSubscribe();  
        //     client.publish(MQTT_INFO.serverToWebDestination,veriler.veri)
        // }
        if(islem == "NewDepoAdded"){
            let serial = message[2]
            devices_subscribe(serial)
            setTimeout(() => {
                DeviceSubscribe();
            }, 1000);
            console.log("Yeni Depo Eklendi")
        }else if(islem == "NewKuyuAdded"){
            let serial = message[2]
            devices_subscribe(serial)
            setTimeout(() => {
                DeviceSubscribe();
            }, 1000);
            console.log("Yeni Kuyu Eklendi")
            client.publish(MQTT_INFO.serverToWebDestination,veriler.veri)
        }else if(islem == "DeleteDepo"){
            let serial = message[2]
            cihazlarin_aboneliginden_cik(serial)
            setTimeout(() => {
                DeviceSubscribe();
            }, 1000);
            console.log("Depo Silindi")
            client.publish(MQTT_INFO.serverToWebDestination,veriler.veri)
        }else if(islem == "DeleteKuyu"){
            let serial = message[2]
            cihazlarin_aboneliginden_cik(serial)
            setTimeout(() => {
                DeviceSubscribe();
            }, 1000);
            console.log("Kuyu Silindi")
            client.publish(MQTT_INFO.serverToWebDestination,veriler.veri)
        }else if(islem == "devicesSettingsUpdated"){
            let serial = message[2]
            console.log(serial);
            setTimeout(() => {
                DeviceSubscribe();
            }, 1000);
            console.log(serial+ "nolu Cihazın Ayarları Güncellendi")
            client.publish(MQTT_INFO.serverToWebDestination,veriler.veri)
        }
    }else{
        let konu_ayrim_noktasi = veriler.konu.split("/")[1];
        if(konu_ayrim_noktasi == "DKW"){
            konu_yonlendir(veriler.konu);
        }
    }
}
function cihazlarin_aboneliginden_cik(serial) {
   
    let cihazlarinKonuAdi = serial+"/DK/RES";
    let yoneticilerinKonuAdi = serial+ "/DKW/CMD";
    let serverKonuAdi = serial+"/DKS/CMD";

    client.unsubscribe(cihazlarinKonuAdi);
    client.unsubscribe(yoneticilerinKonuAdi);
    client.unsubscribe(serverKonuAdi);
    console.log("");
    console.log("---------------------------------------------");
    console.log(serial+" numaralı cihaz konuları takipten çıkarıldı.");
    console.log("---------------------------------------------");
    console.log("");
}
function devices_subscribe(serial){
    let cihazlarinKonuAdi = serial+"/DK/RES";
    let yoneticilerinKonuAdi = serial+"/DKW/CMD";
    let serverKonuAdi = serial+"/DKS/CMD";

    client.subscribe(cihazlarinKonuAdi); 
    client.subscribe(yoneticilerinKonuAdi);
    client.subscribe(serverKonuAdi);

    
    console.log("");
    console.log("---------------------------------------------");
    console.log(serial+" numaralı cihaz konuları takibe alındı.");
    console.log("---------------------------------------------");
    console.log("");
}
function all_listen_subject(){
    client.subscribe("busratestnode");
    client.subscribe(MQTT_INFO.webToServerDestination)
    console.log("");
    console.log("---------------------------------------------");
    console.log(MQTT_INFO.webToServerDestination+" konusu takibe alındı.");
    console.log("---------------------------------------------");
    console.log("");
    
    // getDevicesSerial();
} 
function mesajNeredenGeldi (topic,mesaj){
    var konu_parcala = topic.split("/");
    if(konu_parcala[1]== "DKW" && konu_parcala[2]=="CMD"){
        var yenikonu = konu_yonlendir(topic)
        client.publish(yenikonu,mesaj)
        console.log(yenikonu)
    }else if(konu_parcala[1] == "DK" && konu_parcala[2]=="RES"){
        var strMesaj = mesaj.toString()
        switch (strMesaj){
            case "on" :
                depoyaGoreKuyuListesi(konu_parcala[0]).then(kuyulist=>{
                    kuyulist.forEach(kuyuserial => {
                        let konu = kuyuserial+"/DK/CMD";
                        client.publish(konu,"on")
                        if(!acikKuyu[kuyuserial]){
                            acikKuyu[kuyuserial] = {}
                        }
                        if(!acikKuyu[kuyuserial][konu_parcala[0]]){
                            acikKuyu[kuyuserial][konu_parcala[0]] = true
                        }
                        acikKuyu[kuyuserial][konu_parcala[0]] = true
                        
                        kuyuyaGoreDepoListesi(kuyuserial).then(depolist => {
                            depolist.forEach(deposerial => {
                                if(!acikKuyu[kuyuserial][deposerial]){
                                    acikKuyu[kuyuserial][deposerial]=false;
                                }
                            });
                        })
                    });
                })
                break;
            case "off" :
                depoyaGoreKuyuListesi(konu_parcala[0]).then(kuyulist=>{
                    kuyulist.forEach(kuyuserial => {
                        if(!acikKuyu[kuyuserial]){
                            acikKuyu[kuyuserial] = {}
                        }
                        if(!acikKuyu[kuyuserial][konu_parcala[0]]){
                            acikKuyu[kuyuserial][konu_parcala[0]] = false
                        }
                        acikKuyu[kuyuserial][konu_parcala[0]] = false
                        kuyuyaGoreDepoListesi(kuyuserial).then(depolist =>{
                            depolist.forEach(deposerial => {
                                if(!acikKuyu[kuyuserial][deposerial]){
                                    acikKuyu[kuyuserial][deposerial]=false;
                                }
                            });
                        })
                        kuyu_kapansin_mi(kuyuserial).then(sonuc =>{
                            if(sonuc == true){
                                let konu = kuyuserial+"/DK/CMD";
                                client.publish(konu,"off")
                            }
                        })
                    });
                })
                break;
            default :
                var yenikonu = konu_yonlendir(topic)
                client.publish(yenikonu,mesaj)
                console.log(yenikonu)
                break;
        }
    }
}
function kuyu_kapansin_mi(serial){
    return kuyuyaGoreDepoListesi(serial).then(depolist=>{
        var falsecount = 0;
        depolist.forEach(deposerial => {
            if(!acikKuyu[serial]){
                acikKuyu[serial] = {}
                if(!acikKuyu[serial][deposerial]){
                    acikKuyu[serial][deposerial] = false
                }
            }
            if(acikKuyu[serial][deposerial] == false){
                falsecount++
            }
        });
        //  console.log({depolist,falsecount,acikKuyu})
        if(falsecount == depolist.length){
            
            return true
        }
        else{
            
            return false
        }
    })
}
function depoyaGoreKuyuListesi (serial){
    var list = new Array()
    depokuyu.forEach(element => {
        if(element.depo.serial == serial){
            list.push(element.kuyu.serial)
        }
    })
    return new Promise((resolve, reject) => {
        resolve(list);
        // or
        // reject(new Error("Error!"));
      });
}
function kuyuyaGoreDepoListesi (serial){
    var list = new Array()
    depokuyu.forEach(element => {
        if(element.kuyu.serial == serial){
            list.push(element.depo.serial)
        }
    })
    return new Promise((resolve, reject) => {
        resolve(list);
        // or
        // reject(new Error("Error!"));
      });
}
function mesajDeviceReceive(topic,message){ //cihaza yollanan komutu yakalayıp dönen fonksiyon
    var konu_parcala = topic.split("/");
    if(konu_parcala[1]== "DKW" && konu_parcala[2]=="CMD"){
        var yenikonu = konu_yonlendir(topic)
        client.publish(yenikonu,message)
        console.log(yenikonu)
    }else if(konu_parcala[1] == "DK" && konu_parcala[2]=="RES"){
        var strMesaj = message.toString();
        if(strMesaj == "canli"){
            cihazlardancevapgelirse(topic,message)
            console.log("canlılık durumu geldi");
        }
        
    }
}
// function seviyeDeviceReceive(topic,message){
//     var konu_parcala = topic.split("/");
//     if(konu_parcala[1]== "DKW" && konu_parcala[2]=="CMD"){
//         var yenikonu = konu_yonlendir(topic)
//         client.publish(yenikonu,message)
//         console.log(yenikonu)
//     }else if(konu_parcala[1] == "DK" && konu_parcala[2]=="RES"){
//         seviyeSorgusuGelirse(topic,message)
//         console.log("Seviye Sorgusu Geldi")
//     }
// }
function dakikaya_cevir(saat,dakika){
    return (saat*60) +dakika;
}
function saat_aralik_kontrolu(suan,serial){
    // console.log(devices[serial])
    let time = new Date(suan);
    let saat = time.getHours();
    let dakika = time.getMinutes();
    let suan_toplam_dakika = dakikaya_cevir(saat,dakika);
    let saat_ve_dakika = saat+"."+dakika; //suan 8.51 farzedelim 531 dk

    let baslangic = {
        saat:null,
        dakika:null,
        baslangic_toplam_dakika:null
    }
    baslangic.saat = +devices[serial].baslangic_saati.split(".")[0];
    baslangic.dakika = +devices[serial].baslangic_saati.split(".")[1];
    baslangic.baslangic_toplam_dakika =dakikaya_cevir(baslangic.saat, baslangic.dakika);

    let bitis = {
        saat:null,
        dakika:null,
        bitis_toplam_dakika:null
    }
    bitis.saat = +devices[serial].bitis_saati.split(".")[0];
    bitis.dakika = +devices[serial].bitis_saati.split(".")[1];
    bitis.bitis_toplam_dakika =dakikaya_cevir(bitis.saat,bitis.dakika);

   // bitis.baslangic_toplam_dakika =dakikaya_cevir(devices[serial].bitis_saati.split(".")[0],devices[serial].bitis_dakika.split(".")[1]);

    if(suan_toplam_dakika >= baslangic.baslangic_toplam_dakika && suan_toplam_dakika <= bitis.bitis_toplam_dakika && devices[serial].messagedurum){

        devices[serial].messagedurum =false;
        if(devices[serial].phone1 && devices[serial].phone1!= "" || devices[serial].email1 && devices[serial].email1!= ""){

            // sms.smsGonder({
            //     message:serial+ "seri nolu" +devices[serial].name+ " cihazınıza son" +devices[serial].control_time+ " dk'dır Ulaşılamıyor",
            //     phone:devices[serial].phone1
    
            // });
            func.sendEmail(serial+" seri nolu" +devices[serial].name+ " cihazınıza son" +devices[serial].control_time+ " dk'dır Ulaşılamıyor",devices[serial].email1);
           
        }
        if(devices[serial].phone2 && devices[serial].phone2!= "" || devices[serial].email2 && devices[serial].email2!= ""){

            // sms.smsGonder({
            //     message:serial+" seri nolu" +devices[serial].name+ " cihazınıza son" +devices[serial].control_time+ " dk'dır Ulaşılamıyor",
            //     phone:devices[serial].phone2
    
            // });
            func.sendEmail(serial+" seri nolu " +devices[serial].name+ " cihazınıza son " +devices[serial].control_time+ " dk'dır Ulaşılamıyor",devices[serial].email2);
            
        }
        if(devices[serial].phone3 && devices[serial].phone3!= ""){ //|| devices[serial].email1 && devices[serial].email1!= ""){

            // sms.smsGonder({
            //     message:serial+" seri nolu" +devices[serial].name+ "cihazınıza son" +devices[serial].control_time+ " dk'dır Ulaşılamıyor",
            //     phone:devices[serial].phone3
    
            // });
            // func.sendEmail(serial+" seri nolu cihazınıza son" +devices[serial].control_time+ " dk Ulaşılamıyor",devices[serial].email1);

        }
    }else{
        console.log(serial+"gonderme")
    }
}
function konu_yonlendir(topic){
    var konu_parcala = topic.split("/");
    if(konu_parcala[1]== "DKW"){
        konu_parcala[1]="DK"
    }else if(konu_parcala[1] == "DK"){
        konu_parcala[1] = "DKW"
    }
    return konu_parcala[0]+"/"+konu_parcala[1] +"/"+ konu_parcala[2]
} 
function zaman_fark_kontrol(){
    let suan = new Date().getTime();
    set.forEach(serial => {
        if(devices[serial]){
            let last_time = devices[serial].last_time;
                let fark = (suan - last_time);
                let ctrl_time = new Date(Number(devices[serial].control_time)*1000*60).getTime()
                // console.log({serial,suan,last_time,fark,ctrl_time,control_time:devices[serial].control_time})
                if(fark >= ctrl_time){
                    checkStatus(serial)
                    if(fark >= (ctrl_time+1000*60*1)){ //1 5 OLACAK
                        saat_aralik_kontrolu(suan,serial);
                    }
                    // console.log({serial,fark})
                }
        }
        
    });
}
// function seviyeSorgula(topic){
//     set.forEach(serial => {
//         seviyeStatus(serial)
//     if(devices[serial]){
        
//         }
//     })
// }
function funcinterval(){ //Canlılık Sorgusu için
    statusinterval = setInterval(function(){
        console.log("sorgulanıyor");
        zaman_fark_kontrol()
    },10000)
}
// function Funcinterval(){ //Saniyede bir seviye sorgsu
//     levelinterval = setInterval(function(){
//         console.log("Seviye Sorgusu Gitti");
//         seviyeSorgula()
//     },1000)
// }
function checkStatus(serial){
    client.publish(serial+ "/DK/CMD", "DURUM?");  
}
// function seviyeStatus(serial){
//     client.publish(serial+ "/DK/CMD", "seviye");
// }
function cihazlardancevapgelirse(topic,message){//canlılık durumu gelirse kimden geliyorsa onun serialini al
    let serial = func.getDevicesSerial(topic);
    if(serial &&  devicesettings[serial]){
        let suan =  new Date().getTime();
            devices[serial].last_time = suan;
            devices[serial].messagedurum =true;
    
    }
}
// function seviyeSorgusuGelirse(topic,message){
//     let serial = func.getDevicesSerial(topic);
//     if(serial && devicesettings[serial]){
//         let seviyemsj = message.toString().split("-");
//         devices[serial].seviye = seviyemsj[1];
//          console.log(seviyemsj);
//     }

// }
client.on('message',(topic,message)=>{
    var string = message.toString()
    console.log({topic,string})
    mesajNeredenGeldi(topic,message)
     var veriler = {konu: topic, veri: message};
    // console.log(veriler);
    istekleri_ayir(veriler);
    mesajDeviceReceive(topic,message);
    // seviyeDeviceReceive(topic,message);

})
client.on('connect',()=>{ 
    console.log('connected')
    funcinterval();
    // Funcinterval();
    all_listen_subject();
    DepoKuyu();
    DeviceSubscribe();
    DeviceSettings();
    // yoneticiye_yonlendirmeden_once_yapilacaklar(topic, message)
})
module.exports = {MQTT_INFO:MQTT_INFO};