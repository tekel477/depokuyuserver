var mail = require('./mail');
module.exports={

    getDevicesSerial: function(konu_adi){
        let serial = "";
        if(konu_adi && konu_adi.length > 0){
            let konu_adi_parcala = konu_adi.split("/");
            if(konu_adi_parcala && konu_adi_parcala[0]){
                return konu_adi_parcala[0];
            }else{
                return serial;
            }
        }else{
            return serial;
        }
    },
    sendEmail: function(mailContent,mailAdress){
        mail.send({
            from:"argerftest@gmail.com",
            to:mailAdress,
            subject:"Cihaz Ulaşılabilirlik Uyarı Servisi",
            html:mailContent
        })
    }
}