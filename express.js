var http = require('http');
var express = require('express');
var bodyParser = require('body-parser');

var devices = [{
    serial : "18683250288128471"

},{
    serial : "18683250298120071"
},{
    serial : "18683250298120271"
},{
    serial : "18683250298130171"
}];
 
var app = express();
 
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
 
app.listen(1923, function(){
   console.log("Port dinleniyor 1923...");
});

app.get('/api/devices',function(request,response){
    response.send(devices);
});